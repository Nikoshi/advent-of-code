module Part2
  function solve(prog):: Int
    i = 0; Σ = 0
    for char ∈ prog
      i += 1
      Σ += (Int(char) & 1) == 0 ? 1 : -1
      Σ == -1 && return i
    end
    Σ
  end
end