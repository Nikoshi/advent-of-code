#!/bin/env julia
using Test

module Day01
  include("part1.jl")
  include("part2.jl")
end

file = open("input", "r")
prog = read(file, String)
close(file)

@time @testset "Day 1" begin
  @test Day01.Part1.solve(prog) == 280
  @test Day01.Part2.solve(prog) == 1797
end