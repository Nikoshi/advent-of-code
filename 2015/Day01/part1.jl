module Part1
  function solve(prog):: Int
    Σ = 0
    for char ∈ prog
      Σ += (Int(char) & 1) == 0 ? 1 : -1
    end
    Σ
  end
end