#!/bin/env julia
using Test

module Day04
  include("part1.jl")
  include("part2.jl")
end

file = open("input", "r")
prog = read(file, String)

# Warning Part 2 takes about 27 seconds

@time @testset "Day 4" begin
  @test Day04.Part1.solve(prog) == 346386
  @test Day04.Part2.solve(prog) == 9958218
end