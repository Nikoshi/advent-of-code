module Part1
  using MD5
  
  function solve(prog):: Int
    counter = 0
    hash = ""

    while true
      hash = map(hex, MD5.md5(prog * string(counter))) |> join
      startswith(hash, "00000") && break
      counter+= 1
    end
    
    counter
  end

  hex(n) = string(n, base=16, pad=2)
end