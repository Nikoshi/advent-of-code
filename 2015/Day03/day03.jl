#!/bin/env julia
using Test

module Day03
  include("part1.jl")
  include("part2.jl")
end

file = open("input", "r")
prog = read(file, String)

@time @testset "Day 3" begin
  @test Day03.Part1.solve(prog) == 2081
  @test Day03.Part2.solve(prog) == 2341
end