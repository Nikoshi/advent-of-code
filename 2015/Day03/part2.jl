module Part2
  function solve(prog):: Int
    x_santa = 0; y_santa = 0
    x_robot = 0; y_robot = 0

    houses = [(0,0)]

    index = 0
    for char ∈ prog
      if char == '^'
        index & 1 == 1 ? y_santa += 1 : y_robot += 1
      elseif char == 'v'
        index & 1 == 1 ? y_santa -= 1 : y_robot -= 1
      elseif char == '>'
        index & 1 == 1 ? x_santa += 1 : x_robot += 1
      elseif char == '<'
        index & 1 == 1 ? x_santa -= 1 : x_robot -= 1
      end

      index & 1 == 1 ? append!(houses, [(x_santa,y_santa)]) : append!(houses, [(x_robot,y_robot)])
      index += 1
    end

    unique!(houses)
    length(houses)
  end
end