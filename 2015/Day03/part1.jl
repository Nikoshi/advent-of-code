module Part1
  function solve(prog):: Int
    x = 0; y = 0
    houses = [(x,y)]
    
    for char ∈ prog
      if char == '^'
        y += 1
      elseif char == 'v'
        y -= 1
      elseif char == '>'
        x += 1
      elseif char == '<'
        x -= 1
      end

      append!(houses, [(x,y)])
    end

    unique!(houses)
    length(houses)
  end
end