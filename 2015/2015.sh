#!/bin/bash

time for file in Day*
do
  cd $file
  julia day*.jl
  cd ..
  echo ""
done