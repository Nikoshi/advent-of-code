#!/bin/env julia
using Test

module Day02
  include("part1.jl")
  include("part2.jl")
end

file = open("input", "r")
prog = readlines(file)

@time @testset "Day 2" begin
  @test Day02.Part1.solve(prog) == 1598415
  @test Day02.Part2.solve(prog) == 3812909
end