module Part1
  function solve(prog):: Int
    sum([calculate_paper(get_whl(line)...) for line ∈ prog])
  end

  # Extract width, height and length out of string
  get_whl(str) = match(r"(\d+)x(\d+)x(\d+)", str) |> m -> (w=parse(Int,m[1]), h=parse(Int,m[2]), l=parse(Int,m[3]))

  # calculate area of needed paper
  calculate_paper(w:: Number, h:: Number, l:: Number) = 2sum([w*h h*l l*w]) + minimum([w*h h*l l*w])
end