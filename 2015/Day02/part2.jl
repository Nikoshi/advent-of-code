module Part2
  function solve(prog):: Int
    sum([calculate_ribbon(whl...) for whl ∈ [get_whl(line) for line ∈ prog]])
  end

  # Extract width, height and length out of string
  get_whl(str) = match(r"(\d+)x(\d+)x(\d+)", str) |> m -> (w=parse(Int,m[1]), h=parse(Int,m[2]), l=parse(Int,m[3]))

  # calculate length of needed ribbon
  calculate_ribbon(w, h, l) = (2(sum([w h l]) - maximum([w h l]))) + w*h*l
end