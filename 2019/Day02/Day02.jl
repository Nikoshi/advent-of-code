#!/bin/env julia
using Test

module Day02
  include("part1.jl")
  include("part2.jl")
end

file = open("input", "r")
content = read(file, String)
close(file)

prog = [parse(Int, x) for x in split(content, ",")]

prog[2] = 12
prog[3] = 2

@time @testset "Day 1" begin
  @test Day02.Part1.solve(prog) == 2890696
#   @test Day01.Part2.solve(prog) == 5026151
end
