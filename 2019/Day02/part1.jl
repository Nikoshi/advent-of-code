module Part1
  function solve(prog):: Int
    pc = 1

    while true
      result = perform(pc, prog)

      if result == 0
        # Everything is fine
      elseif result == 1
        # Received 99
        break
      else
        println("Unexpected opcode $result at $(pc-1)")
        break
      end

      pc += 4
    end

    prog[1]
  end

  function perform(pc, prog)
    opcode = prog[pc]
    
    if opcode == 99
      1
    elseif opcode == 1 || opcode == 2
      pos1 = prog[pc + 1] +1
      pos2 = prog[pc + 2] +1
      out = prog[pc + 3] +1

      if opcode == 1
        prog[out] = prog[pos1] + prog[pos2]
        0
      else
        prog[out] = prog[pos1] * prog[pos2]
        0
      end
    else
      pc
    end
  end

end
