module Part1
  function solve(prog):: Int
    sum(sum, [calc_fuel(parse(Int64, line)) for line in prog])
  end

  calc_fuel(mass):: Int = floor(Int, mass / 3) - 2
end
