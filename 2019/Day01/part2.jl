module Part2
  function solve(prog):: Int
    Σ = 0
    for line in prog
      fuel = calc_fuel(parse(Int64, line))
      Σ += fuel
      while fuel >= 0
        fuel = calc_fuel(fuel)
        if fuel > 0
          Σ += fuel
        end
      end
    end
    Σ
  end

  calc_fuel(mass):: Int = floor(Int, mass / 3) - 2
end
