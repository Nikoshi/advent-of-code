#!/bin/bash

time for file in Day*
do
  cd $file
  julia Day*.jl
  cd ..
  echo ""
done
